import { IsNotEmpty, IsNumber, IsString } from "class-validator";

enum PERMISSION {
  ADMIN = 1,
  NORMAL = 2
}


export class CreateUserDto {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsString()
    address: string;

    @IsNotEmpty()
    @IsNumber()
    permission: PERMISSION;
  }