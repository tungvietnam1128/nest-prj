import { Body, Controller, Get, HttpException, HttpStatus, Param, Query, Post, UsePipes, ParseUUIDPipe, ParseArrayPipe } from '@nestjs/common';
import { User } from 'src/models/user.model';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private userService: UsersService){}

    @Post()
    async createUser(@Body() createUserDTO: CreateUserDto): Promise<User>{
        return await this.userService.create(createUserDTO)
    }

    @Get(':uid')
    async getUserById(@Param('uid') uid: string): Promise<User>{
        return await this.userService.findById(uid)
    }

    @Get()
    async getUsersByIds(@Query('uids', new ParseArrayPipe({items: String, separator: ','})) ids: string[]): Promise<User[]>{
        return this.userService.findByUids(ids)
    }
}
