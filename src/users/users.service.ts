import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDocument, User } from 'src/models/user.model';
import { CreateUserDto } from './dto/create-user.dto';
import { v4 as uuid } from 'uuid';
import { isEmpty } from 'class-validator';

@Injectable()
export class UsersService {
    constructor(@InjectModel('user') private userModel: Model<UserDocument>){}

    async create(userDto: CreateUserDto): Promise<User>{
        const user = await this.userModel.findOne({ name: userDto.name })
        if(user)
            throw new HttpException('User already exist', HttpStatus.BAD_GATEWAY)

        const newUser = new this.userModel({uid: uuid(), ...userDto})
        return await newUser.save()
    }

    async findById(uid: string): Promise<User>{
        const user = await this.userModel.findOne({ uid })
        if(!user)
            throw new HttpException('User not exist', HttpStatus.NOT_FOUND)

        return user
    }

    async findByUids(uids: string[]): Promise<User[]>{
        const users = await this.userModel.find({uid: {$in : uids}})
        if(isEmpty(users))
            throw new HttpException('Users not exist', HttpStatus.NOT_FOUND)
        
        return users
    }
}
