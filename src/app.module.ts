import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from './app.service'
import { AppController} from './app.controller'
import { ConfigModule } from '@nestjs/config';
import { UsersModule} from './users'
import { APP_FILTER, APP_PIPE, APP_INTERCEPTOR } from '@nestjs/core';
import { HttpExceptionFilter } from './core/exception-filters';
import { ClassValidationPipe } from './core/pipes';
import { LoggerMiddleware } from './core/middlewares';
import { LoggerInterceptor, ResponseInterceptor } from './core/interceptors';

const imports = [
  ConfigModule.forRoot({
    envFilePath: '.development.env',
    isGlobal: true
  }),
  MongooseModule.forRoot(process.env.MOONGO_DB_URL),
  UsersModule,
]

const providers = [
  AppService,
  {
    provide: APP_FILTER,
    useClass: HttpExceptionFilter,
  },
  {
    provide: APP_PIPE,
    useClass: ClassValidationPipe
  },
  {
    provide: APP_INTERCEPTOR,
    useClass: ResponseInterceptor
  },
  {
    provide: APP_INTERCEPTOR,
    useClass: LoggerInterceptor
  }
]

@Module({
  imports,
  controllers: [AppController],
  providers,
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes();
  }
  
}
