
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({required: true})
  uid: string
  
  @Prop({required: true})
  name: string;

  @Prop({required: true})
  address: string;

  @Prop()
  permission: number;
}

export const UserSchema = SchemaFactory.createForClass(User);