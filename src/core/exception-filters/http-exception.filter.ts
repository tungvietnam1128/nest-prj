import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
    HttpStatus,
} from '@nestjs/common';

import { Request, Response } from 'express'

@Catch()
export class HttpExceptionFilter implements ExceptionFilter{
    catch(exception: unknown, host: ArgumentsHost): void {
        const ctx = host.switchToHttp()
        const request = ctx.getRequest<Request>()
        const response = ctx.getResponse<Response>();

        const ext = exception instanceof HttpException
            ? { status: exception.getStatus(), message: exception.message }
            : { status: HttpStatus.INTERNAL_SERVER_ERROR, message: 'Internal server error' }

        response.status(ext.status).json({
            timestamp: new Date().toISOString(),
            message: ext.message,
            statusCode: ext.status,
            path: request.url,
        });
    }

}