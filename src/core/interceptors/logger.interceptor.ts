import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Request } from 'express'

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp()
    const req = ctx.getRequest<Request>()
    const now = Date.now();
    return next
      .handle()
      .pipe(tap(() => console.log(`Handle success request '${req.method}: ${req.path}' after... ${Date.now() - now}ms`)),);
  }
}
