import { ObjectSchema } from '@hapi/joi';
import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import { BadRequestException } from '@nestjs/common/exceptions/bad-request.exception';

@Injectable()
export class JoiValidationPipe implements PipeTransform{
    constructor(private schema: ObjectSchema) {}

    transform(value: any, metadata: ArgumentMetadata): void {
        const { error } = this.schema.validate(value);
        if (error) {
            throw new BadRequestException('Validation failed');
        }
        return value;
    }
}